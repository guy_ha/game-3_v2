﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;

namespace Game13
{
    class gameBorder
    {
        private float screenWidth;
        private float screenHeight;
        float borderWidth = 3f;
        private Body[] borderArr;
        private Body bottom;
        private Body left;
        private Body right;
        private Body up;
        


        private World world;

        public gameBorder(World _world,float height,float width)
        {
            world = _world;
            screenHeight = ConvertUnits.ToSimUnits(height);
            screenWidth = ConvertUnits.ToSimUnits(width);


            createBorder();
        }

        public void createBorder()
        {
            Vector2 screenCenter = new Vector2(screenWidth / 2, screenHeight / 2);
            bottom = BodyFactory.CreateRectangle(world, screenHeight, screenWidth, 1f, screenCenter + new Vector2(2, 12.5f),0f,BodyType.Static,null);
            right = BodyFactory.CreateRectangle(world,borderWidth, screenHeight, 1f, screenCenter + new Vector2(14, 0), 0f, BodyType.Static, null);
            left = BodyFactory.CreateRectangle(world, borderWidth, screenHeight, 1f, screenCenter - new Vector2(9, 0), 0f, BodyType.Static, null);
            up = BodyFactory.CreateRectangle(world, screenHeight, screenWidth, 1f, screenCenter - new Vector2(0, 17.5f), 0f, BodyType.Static, null);

            right.Restitution = 1.5f;
            left.Restitution = 1.5f;
            right.Friction = 0.5f;
            left.Friction = 0.5f;

            bottom.CollisionCategories = Category.All;
            bottom.CollidesWith = Category.All;
            right.CollidesWith = Category.All;
            left.CollisionCategories = Category.All;
            right.CollisionCategories = Category.All;

        }

    }
}
