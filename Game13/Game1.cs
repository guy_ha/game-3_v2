﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.DebugView;

namespace Game13
{
   
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        private static GraphicsDevice _graphics;
        SpriteBatch _batch;

        bool hitBtn = false;
        bool win = false;

        //Try
        private SpriteFont gameFont;
        private String findPos;
        private Vector2 TextPos = new Vector2(50, 900);
        private String victory = "You Win";
        private String yPos;







        // Physics
        private World world;
        private const float G = 9.81f;
        private Vector2 gravity = new Vector2(0.0f,G);

        //Controls     
        private KeyboardState _oldKeyState;
        private GamePadState _oldPadState;
        private SpriteFont _font;


        // Simple camera controls
        private Matrix _view;

        // Camera
        private Camera2D camera;

        //Border      
        Border border;

  
        //Textures
        private Texture2D _circleSprite;
        private Texture2D _groundSprite;
        private Texture2D _IcyGroundSprite;
        private Texture2D _IcyGroundSprite2;
        private Texture2D _iceCubeSprite;
        private Texture2D _btnSprite;
        private Texture2D _trophySprite;
        private Texture2D _blockSprite;

        //Body
        private Body Cube;
        private Body _circleBody;
        private Body _groundBody;
        private Body _IcyGroundBody;
        private Body _IcyGroundBody2;
        private Body iceCubeBody;
        private Body btnBody;
        private Body trophyBody;
        private Body BlockBody;
        // private Body[] borders;

        //Position
        private Vector2 _cameraPosition;
        private Vector2 _screenCenter;
        private Vector2 _groundOrigin;
        private Vector2 _groundOrigin2;
        private Vector2 _groundOrigin3;
        private Vector2 _circleOrigin;
        private Vector2 _IcyGroundOrigin;
        private Vector2 _iceCubeOrigin;
        private Vector2 _btnOrigin;
        private Vector2 _trophyOrigin;
        private Vector2 _blockOrigin;
       


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            //graphics.SynchronizeWithVerticalRetrace = false;
            //graphics.PreferMultiSampling = true;
            //IsFixedTimeStep = true;

            //Screen resolution
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 960;

            Content.RootDirectory = "Content";

            _graphics = GraphicsDevice;

            world = new World(gravity);

                             
        }

       
        protected override void Initialize()
        {        
            base.Initialize();
        }

    

        protected override void LoadContent()
        {
            camera = new Camera2D(GraphicsDevice);

            //Font
            gameFont = Content.Load<SpriteFont>("Font");

            _view = Matrix.Identity;
            _cameraPosition = Vector2.Zero;
            _screenCenter = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2f, graphics.GraphicsDevice.Viewport.Height / 2f);
            _batch = new SpriteBatch(GraphicsDevice);
            _batch = new SpriteBatch(GraphicsDevice);
            Vector2 gameWorld = new Vector2(1280, 960);
             
             
           
            border = new Border(world, gameWorld.X, gameWorld.Y, 1f);


            // Load sprites
            _circleSprite = Content.Load<Texture2D>("CircleSprite"); //  96px x 96px => 1.5m x 1.5m
            _groundSprite = Content.Load<Texture2D>("GroundSprite"); // 512px x 64px =>   8m x 1m
            _IcyGroundSprite = Content.Load<Texture2D>("IcyGroundSprite2");
            _IcyGroundSprite2 = Content.Load<Texture2D>("IcyGroundSprite");
            _iceCubeSprite = Content.Load<Texture2D>("IceCube");
            _btnSprite = Content.Load<Texture2D>("btn");
            _trophySprite = Content.Load<Texture2D>("trophy");
            _blockSprite = Content.Load<Texture2D>("Block");

            /* We need XNA to draw the ground and circle at the center of the shapes */
            _groundOrigin = new Vector2(_groundSprite.Width / 2f, _groundSprite.Height / 2f);
            _circleOrigin = new Vector2(_circleSprite.Width / 2f, _circleSprite.Height / 2f);
            _IcyGroundOrigin = new Vector2(_IcyGroundSprite.Width / 2f, (_IcyGroundSprite.Height / 2f));
            _iceCubeOrigin = new Vector2(_iceCubeSprite.Width / 2, _iceCubeSprite.Height / 2);
            _btnOrigin = new Vector2(_btnSprite.Width / 2, _btnSprite.Height / 2);
            _trophyOrigin = new Vector2(_trophySprite.Width / 2, _trophySprite.Height / 2);
            _blockOrigin = new Vector2(_blockSprite.Width / 2, _blockSprite.Height / 2);
            //_IcyGroundOrigin2 = new Vector2(_IcyGroundSprite.Width / 2f, (_IcyGroundSprite.Height / 2f));

            // Velcro Physics expects objects to be scaled to MKS (meters, kilos, seconds)
            // 1 meters equals 64 pixels here
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);

            /* Circle */
            // Convert screen center from pixels to meters
            Vector2 circlePosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(-6, -2.5f);
            Vector2 iceCubePosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(0,4.5f);
            Vector2 btnPosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(8.75f, 6.5f);
            

            // Create the circle fixture
            _circleBody = BodyFactory.CreateCircle(world, ConvertUnits.ToSimUnits(96 / 2f), 1f, circlePosition, BodyType.Dynamic);
            _circleBody.CollidesWith = Category.All;
            _circleBody.CollisionCategories = Category.All;

            // Give it some bounce and friction
            _circleBody.Restitution = 0.3f;
            _circleBody.Friction = 0.5f;

            //Create the ice cube fixture
            iceCubeBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(141/2f), ConvertUnits.ToSimUnits(156/2f), 2f,iceCubePosition,0f, BodyType.Dynamic);
            iceCubeBody.Restitution = 0.5f;
            iceCubeBody.Friction = 0.5f;
            iceCubeBody.CollisionCategories = Category.Cat1;
            iceCubeBody.CollidesWith = Category.Cat2;
          

            //Create the btn
            btnBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(140/2), 1f, 0f, btnPosition, 0f, BodyType.Static);
            btnBody.CollisionCategories = Category.Cat2;
            btnBody.CollidesWith = Category.Cat1;

            //Create Block
            Vector2 blockPosition = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(-7f, 6.3f);
            BlockBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(64f), ConvertUnits.ToSimUnits(244f), 1f, blockPosition);


            /* Ground */
            Vector2 groundPosition = ConvertUnits.ToSimUnits(_screenCenter) + new Vector2(6, 4f);
            Vector2 groundPosition2 = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(4, 3.0f);
            Vector2 groundPosition3 = ConvertUnits.ToSimUnits(_screenCenter) -new Vector2(-6, 4f);
            Vector2 icyGroundPosition = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(0, 5.5f);
            Vector2 icyGroundPosition2 = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(0, 5.5f);
            findPos =icyGroundPosition2.ToString();
            float x= ConvertUnits.ToSimUnits(1280f);
            float y = ConvertUnits.ToSimUnits(960f);
            //xPos = x.ToString();
            yPos = "Push the Ice Cube to the Blue BTN to open the door";


            // Create the ground fixture
            _groundBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(512f), ConvertUnits.ToSimUnits(64f), 1f, groundPosition);
            _groundBody.BodyType = BodyType.Static;
            _groundBody.Restitution = 0.2f;
            _groundBody.Friction = 0.2f;

            //Creata the Trophy fixture
            Vector2 trophyPos = ConvertUnits.ToSimUnits(_screenCenter) - new Vector2(-9f, 5.1f);
            trophyBody = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(103), ConvertUnits.ToSimUnits(80), 1f, trophyPos);

            Floor second = new Floor(groundPosition2,world);
            _groundOrigin2 = second.getLocation();

            Floor third = new Floor(groundPosition3, world);
            _groundOrigin3 = third.getLocation();

            gameBorder bord = new gameBorder(world,1280f,960f);
           
        }

       

        protected override void UnloadContent()
        {
            
        }

        
        protected override void Update(GameTime gameTime)
        {
            //iceCubeBody.ApplyForce(new Vector2(-10, 0));
            //if (btnBody.CollidesWith))
            //{

            //}
            // iceCubeBody.ApplyLinearImpulse(new Vector2(-40,0));
            float hit = Vector2.Distance(iceCubeBody.Position, btnBody.Position)-1.5f;
            if (hit < 0.2)
                {
                hitBtn = true;
                BlockBody.IgnoreCollisionWith(_circleBody);
                };
            float winGame = Vector2.Distance(_circleBody.Position, trophyBody.Position) - 1.5f;
            if(winGame < 0.2)
                {
                win = true;
                };


            HandleGamePad();
            HandleKeyboard();

            //We update the world
            world.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);

            base.Update(gameTime);
        }


        private void HandleGamePad()
        {
            GamePadState padState = GamePad.GetState(0);

            if (padState.IsConnected)
            {
                if (padState.Buttons.Back == ButtonState.Pressed)
                    Exit();

                if (padState.Buttons.A == ButtonState.Pressed && _oldPadState.Buttons.A == ButtonState.Released)
                    _circleBody.ApplyLinearImpulse(new Vector2(0, -10));

                _circleBody.ApplyForce(padState.ThumbSticks.Left);
                _cameraPosition.X -= padState.ThumbSticks.Right.X;
                _cameraPosition.Y += padState.ThumbSticks.Right.Y;

                _view = Matrix.CreateTranslation(new Vector3(_cameraPosition - _screenCenter, 0f)) * Matrix.CreateTranslation(new Vector3(_screenCenter, 0f));

                _oldPadState = padState;
            }
        }

        private void HandleKeyboard()
        {
            KeyboardState state = Keyboard.GetState();

            // Move camera
            if (state.IsKeyDown(Keys.Left))
                _cameraPosition.X += 1.5f;

            if (state.IsKeyDown(Keys.Right))
                _cameraPosition.X -= 1.5f;

            if (state.IsKeyDown(Keys.Up))
                _cameraPosition.Y += 1.5f;

            if (state.IsKeyDown(Keys.Down))
                _cameraPosition.Y -= 1.5f;

            _view = Matrix.CreateTranslation(new Vector3(_cameraPosition - _screenCenter, 0f)) * Matrix.CreateTranslation(new Vector3(_screenCenter, 0f));

            // We make it possible to rotate the circle body
            if (state.IsKeyDown(Keys.A))
                _circleBody.ApplyTorque(-10);

            if (state.IsKeyDown(Keys.D))
                _circleBody.ApplyTorque(10);



            if (state.IsKeyDown(Keys.Space) && _oldKeyState.IsKeyUp(Keys.Space))
                _circleBody.ApplyLinearImpulse(new Vector2(0, -10));

            if (state.IsKeyDown(Keys.Escape))
                Exit();

            _oldKeyState = state;
        }

     

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _batch.Begin(SpriteSortMode.Deferred,null,null, null,null,null,_view);
           //_batch.DrawString(gameFont, findPos, TextPos, Color.Black);
            if (win){
                _batch.DrawString(gameFont, victory, new Vector2(50, 100), Color.Black);
            };
           
            _batch.DrawString(gameFont, yPos, new Vector2(50, 200), Color.Black);
            _batch.Draw(_btnSprite, ConvertUnits.ToDisplayUnits(btnBody.Position), null, Color.White, btnBody.Rotation, _btnOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_circleSprite, ConvertUnits.ToDisplayUnits(_circleBody.Position), null, Color.White, _circleBody.Rotation, _circleOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_iceCubeSprite, ConvertUnits.ToDisplayUnits(iceCubeBody.Position),null, Color.White,iceCubeBody.Rotation,_iceCubeOrigin,1f,SpriteEffects.None,0f);
            _batch.Draw(_groundSprite, ConvertUnits.ToDisplayUnits(_groundBody.Position), null, Color.White, 0f, _groundOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_groundSprite, ConvertUnits.ToDisplayUnits(_groundOrigin2), null, Color.White, 0f, _groundOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_groundSprite, ConvertUnits.ToDisplayUnits(_groundOrigin3), null, Color.White, 0f, _groundOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_trophySprite, ConvertUnits.ToDisplayUnits(trophyBody.Position), null, Color.White, 0f, _trophyOrigin, 1f, SpriteEffects.None, 0f);
            _batch.Draw(_blockSprite, ConvertUnits.ToDisplayUnits(BlockBody.Position), null, Color.White, 0f, _blockOrigin, 1f, SpriteEffects.None, 0f);
            //_batch.Draw(_IcyGroundSprite, ConvertUnits.ToDisplayUnits(_IcyGroundBody.Position), null, Color.White, 0f, _IcyGroundOrigin, 1f, SpriteEffects.None, 0f);
            //_batch.Draw(_IcyGroundSprite2, ConvertUnits.ToDisplayUnits(_IcyGroundBody.Position), null, Color.White, 0f, _IcyGroundOrigin, 1f, SpriteEffects.None, 0f);

            _batch.End();

        


            base.Draw(gameTime);
        }
    }
}
