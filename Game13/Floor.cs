﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics.Common;
using FarseerPhysics.DebugView;

namespace Game13
{
    class Floor
    {
        private Vector2 location;
        private Body floorBody;
        
        public Floor(Vector2 x,World w)
        {
           location = x;
           floorBody = BodyFactory.CreateRectangle(w, ConvertUnits.ToSimUnits(512f), ConvertUnits.ToSimUnits(64f), 1f, location);
           floorBody.BodyType = BodyType.Static;
           floorBody.Restitution = 0.2f;
           floorBody.Friction = 0.2f;
        }

        public Vector2 getLocation()
        {
            return location;
        }
    }
}
